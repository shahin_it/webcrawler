from bs4 import BeautifulSoup
from requests_futures.sessions import FuturesSession

from services.model import HeadLine

session = FuturesSession()


def prothom_alo() -> []:
    base_url = 'https://www.prothomalo.com/'
    req = session.get(base_url + '/collection/latest').result()
    html = req.text
    soup = BeautifulSoup(html, 'lxml')
    stories = soup.body.select_one('.stories-set')
    news = []
    for story in stories.select('.news_item'):
        heading = story.select_one('.tilte-no-link-parent')
        headLine = HeadLine(title=heading.text)
        headLine.source_url = story.select_one('.card-with-image-zoom')['href']
        headLine.published = story.select_one('.published-time').text
        news.append(headLine)
        HeadLine.data[headLine.title] = headLine
    print(base_url + ' loaded..')
    return news


def kaler_kantho() -> []:
    base_url = 'https://www.kalerkantho.com'
    req = session.get(base_url + '/recent').result()
    html = req.content
    soup = BeautifulSoup(html, 'lxml')
    stories = soup.body.select('.all_headline .headline li a')
    news = []
    for story in stories:
        headLine = HeadLine(title=story.contents[0])
        headLine.source_url = base_url + story['href'].replace('./', '/')
        headLine.published = story.select_one('small').text
        news.append(headLine)
        HeadLine.data[headLine.title] = headLine
    print(base_url + ' loaded..')
    return news
