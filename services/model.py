from urllib.parse import urlparse


class HeadLine():
    data = {}

    def __init__(self, title: str, summary: str = ''):
        self.title = title
        self.summary = summary

    title = ''
    summary = ''
    source_url = ''
    published = ''
    image = ''

    @property
    def source(self):
        return urlparse(self.source_url).netloc


class News(HeadLine):
    data = {}

    def __init__(self, title: str, details: str = '', head_line: HeadLine = None):
        if head_line:
            super().__init__(title, head_line.summary)
            self.source_url = head_line.source_url
            self.published = head_line.published
            self.image = head_line.image
        else:
            super().__init__(title)
        self.details = details

    details = ''
    images = []
