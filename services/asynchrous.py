from threading import Thread


def run(targets: [], args=[], sync=False):
    thread = None
    for target in targets:
        thread = Thread(target=target, args=args)
        thread.start()

    if sync:
        thread.join()
