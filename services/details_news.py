from bs4 import BeautifulSoup
from requests_futures.sessions import FuturesSession

from services.model import News, HeadLine

session = FuturesSession()


def prothom_alo() -> []:
    base_url = 'https://www.prothomalo.com/'
    req = session.get(base_url + '/collection/latest').result()
    html = req.text
    soup = BeautifulSoup(html, 'lxml')
    stories = soup.body.select_one('.stories-set')
    news = []
    for story in stories.select('.bn-story-card'):
        heading = story.select_one('.headline')
        headLine = News(title=heading.text)
        headLine.source_url = heading.parent['href']
        headLine.published = story.select_one('.published-time').text
        news.append(headLine)
        News.data[headLine.title] = headLine
    print(base_url + ' loaded..')
    return news


def kaler_kantho(title) -> str:
    news = News.data.get(title)
    if news:
        return news
    else:
        headLine = HeadLine.data.get(title)
        news = News(title=headLine.title, head_line=headLine)
        base_url = news.source_url
        req = session.get(base_url).result()
        soup = BeautifulSoup(req.content, 'lxml')
        story = soup.body.select_one('.details')
        image = story.select_one('.img')
        news.images.append(image['src'])
        for p in story.select('.some-class-name2 p'):
            news.details += str(p)
        News.data[title] = news
        print(base_url + ' loaded..')
    return news
