# app/home/views.py

from flask import render_template
from flask_login import login_required

from services import heading, asynchrous, details_news
from services.model import HeadLine
from . import home


@home.route('/')
def homepage():
    asynchrous.run([heading.prothom_alo, heading.kaler_kantho], sync=len(HeadLine.data) == 0)
    print("thread finished...exiting")
    stories = HeadLine.data.values()  # heading.prothom_alo() + heading.kaler_kantho()
    return render_template('home/index.html', title="Welcome", headings=stories)


@home.route('/details/<title>')
def news_details(title):
    news = details_news.kaler_kantho(title)
    return render_template('home/dashboard.html', news=news)


@home.route('/dashboard')
@login_required
def dashboard():
    """
    Render the dashboard template on the /dashboard route
    """
    return render_template('home/dashboard.html', title="Dashboard")
