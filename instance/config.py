# instance/config.py
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve(strict=True).parents[1]
SECRET_KEY = 'p9Bv<3Eid9%$i01'
# SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/crawler'
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'db.sqlite3')
SQLALCHEMY_TRACK_MODIFICATIONS = True
